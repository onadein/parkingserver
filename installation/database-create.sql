create role parkman;
alter role parkman with nosuperuser inherit nocreaterole nocreatedb login password 'q1w2e3' valid until 'infinity';

create database parking owner = parkman;

\connect parking

CREATE TABLE journal
(
  id serial NOT NULL,
  place_number integer,
  car_govnum text,
  car_model text,
  move_in timestamp,
  move_out timestamp,
  
  CONSTRAINT pk_journal_id PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
alter table journal owner to parkman;

