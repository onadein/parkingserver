package ru.hayova.parking.model;

//**********************************
//* (c) Oleg 'hayova' Nadein, 2016 *
//**********************************

import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "journal")
@NamedQueries({
    @NamedQuery(name = "journal.getAllOnParking", query = "SELECT j FROM Journal j WHERE move_out IS NULL"),
    @NamedQuery(name = "journal.updateCar", query = "UPDATE Journal SET move_out = :moveout WHERE car_govnum = :cargovnum AND car_model = :carmodel"),
    @NamedQuery(name = "journal.getDataForRates", query = "SELECT j FROM Journal j WHERE (j.moveInDate BETWEEN :startDate AND :endDate) OR (j.moveOutDate BETWEEN :startDate AND :endDate) OR (j.moveOutDate IS NULL AND :startDate > j.moveInDate)"),
    @NamedQuery(name = "journal.getCarOnParking", query = "SELECT j FROM Journal j WHERE car_govnum = :cargovnum AND car_model = :carmodel AND move_out IS NULL")
})
public class Journal implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@JsonIgnore
	@Column (name="place_number")
	private Integer placeNumber;
	
	@Column (name="car_govnum")
	private String carGovnum;
	
	@Column (name="car_model")
	private String  carModel;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss", timezone="Europe/Moscow")
	@Column (name="move_in")
	private Date moveInDate;
	
	@JsonIgnore
	@Column (name="move_out")
	private Date moveOutDate;

	public Journal() {
		
	}
	
	public Journal(String carGovnum, String carModel, Date moveInDate) {
		this.carGovnum = carGovnum;
		this.carModel = carModel;
		this.moveInDate = moveInDate;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPlaceNumber() {
		return placeNumber;
	}

	public void setPlaceNumber(Integer placeNumber) {
		this.placeNumber = placeNumber;
	}

	public String getCarGovnum() {
		return carGovnum;
	}

	public void setCarGovnum(String carGovnum) {
		this.carGovnum = carGovnum;
	}

	public String getCarModel() {
		return carModel;
	}

	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}

	public Date getMoveInDate() {
		return moveInDate;
	}

	public void setMoveInDate(Date moveInDate) {
		this.moveInDate = moveInDate;
	}

	public Date getMoveOutDate() {
		return moveOutDate;
	}

	public void setMoveOutDate(Date moveOutDate) {
		this.moveOutDate = moveOutDate;
	}

}
