package ru.hayova.parking.model;

//**********************************
//* (c) Oleg 'hayova' Nadein, 2016 *
//**********************************

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ParkingRequest implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String action;

	private Journal car;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="Europe/Moscow")
	private Date rateBeginDate;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="Europe/Moscow")
	private Date rateEndDate;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Journal getCar() {
		return car;
	}
	public void setCar(Journal car) {
		this.car = car;
	}
	public Date getRateBeginDate() {
		return rateBeginDate;
	}
	public void setRateBeginDate(Date rateBeginDate) {
		this.rateBeginDate = rateBeginDate;
	}
	public Date getRateEndDate() {
		return rateEndDate;
	}
	public void setRateEndDate(Date rateEndDate) {
		this.rateEndDate = rateEndDate;
	}
	
}
