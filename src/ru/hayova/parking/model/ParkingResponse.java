package ru.hayova.parking.model;

//**********************************
//* (c) Oleg 'hayova' Nadein, 2016 *
//**********************************

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

public class ParkingResponse {
	
	private String result;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private ResultData resultData;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private RatesData ratesData;
	
	public RatesData getRatesData() {
		return ratesData;
	}

	public void setRatesData(RatesData ratesData) {
		this.ratesData = ratesData;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public ResultData getResultData() {
		return resultData;
	}

	public void setResultData(ResultData resultData) {
		this.resultData = resultData;
	}
	
	public class ResultData { 
		
		@JsonInclude(JsonInclude.Include.NON_NULL)
		private String description;

		@JsonInclude(JsonInclude.Include.NON_NULL)
		private String info;

		@JsonInclude(JsonInclude.Include.NON_NULL)
		private List<Journal> cars;
		
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getInfo() {
			return info;
		}
		public void setInfo(String info) {
			this.info = info;
		}
		public List<Journal> getCars() {
			return cars;
		}
		public void setCars(List<Journal> cars) {
			this.cars = cars;
		}
	}
	
	public class RatesData {
		private long totalRate;
		List<PerDayRatesData> perDayRates;
		
		public List<PerDayRatesData> getPerDayRates() {
			return perDayRates;
		}

		public void setPerDayRates(List<PerDayRatesData> perDayRates) {
			this.perDayRates = perDayRates;
		}

		public long getTotalRate() {
			return totalRate;
		}

		public void setTotalRate(long totalRate) {
			this.totalRate = totalRate;
		}
		
		public class PerDayRatesData {
			@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="Europe/Moscow")
			private Date date;
			private long rate;
			
			public Date getDate() {
				return date;
			}
			public void setDate(Date date) {
				this.date = date;
			}
			public long getRate() {
				return rate;
			}
			public void setRate(long rate) {
				this.rate = rate;
			}
			 
		}
	}

}
