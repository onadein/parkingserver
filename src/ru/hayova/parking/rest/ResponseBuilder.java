package ru.hayova.parking.rest;

//**********************************
//* (c) Oleg 'hayova' Nadein, 2016 *
//**********************************

import javax.ws.rs.core.Response;

public class ResponseBuilder {

	public static Response error(Response.Status status, int errorCode) {
		Response.ResponseBuilder response = Response.status(status);
		response.header("errorcode", errorCode);
		response.entity("none");
		return response.build();
	}

	public static Response success(Object object) {
		Response.ResponseBuilder response = Response.ok();
		if (object != null) {
			response.entity(object);
		} else {
			response.entity("none");
		}
		return response.build();
	}
}