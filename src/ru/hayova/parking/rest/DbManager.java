package ru.hayova.parking.rest;

//**********************************
//* (c) Oleg 'hayova' Nadein, 2016 *
//**********************************

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ru.hayova.parking.model.Journal;

@Stateless
public class DbManager {

	@PersistenceContext(unitName="parking")
    private EntityManager manager;
    
    @SuppressWarnings("unchecked")
	public List<Journal> getJournalByCarWithEmptyOut(Journal car) {
        return manager.createNamedQuery("journal.getCarOnParking")
                .setParameter("cargovnum", car.getCarGovnum().trim())
                .setParameter("carmodel", car.getCarModel().trim())
                .getResultList();
    }
    
	public void updateJournalByCarOut(Journal car, Date moveOut) {
        manager.createNamedQuery("journal.updateCar")
        		.setParameter("moveout", moveOut)
                .setParameter("cargovnum", car.getCarGovnum().trim())
                .setParameter("carmodel", car.getCarModel().trim())
                .executeUpdate();
    }
	
	@SuppressWarnings("unchecked")
	public List<Journal> getJournalByDatePeriod(Date startDate, Date endDate) {
        return manager.createNamedQuery("journal.getDataForRates")
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<Journal> getJournalsWithEmptyOut() {
        return manager.createNamedQuery("journal.getAllOnParking").getResultList();
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void addJournal(Journal entry) {
        manager.persist(entry);
    }
    
}