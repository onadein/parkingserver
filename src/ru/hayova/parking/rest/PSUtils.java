package ru.hayova.parking.rest;

import java.util.Calendar;
import java.util.Date;

import ru.hayova.parking.model.Journal;

public class PSUtils {
	
	public static boolean testCar(Journal car) {
		if(car == null)
			return false;
		if(car.getCarGovnum() == null || car.getCarModel() == null)
			return false;
		if(car.getCarGovnum().trim().isEmpty() || car.getCarModel().trim().isEmpty())
			return false;
		return true;
	}
	
	public static Date setDateTimeToBegin(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}
	
	public static Date setDateTimeToEnd(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 999);
		return c.getTime();
	}

	public static Date addOneDayToDate(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, 1);
		return c.getTime();
	}
	
}
