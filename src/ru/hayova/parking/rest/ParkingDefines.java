package ru.hayova.parking.rest;

//**********************************
//* (c) Oleg 'hayova' Nadein, 2016 *
//**********************************

public class ParkingDefines {
	public static final Integer MAX_PARKING_PLACES = 10;
	
	public static final Integer PER_HOUR_RATE_TAX = 100;
	
	public static final String RESPONSE_RESULT_SUCCESS = "success";
	public static final String RESPONSE_RESULT_ERROR = "error";
	
	public static final String REQUEST_ACTION_GET_FREE_SPACE = "get_free_space";
	public static final String REQUEST_ACTION_ADD_CAR = "add_car";
	public static final String REQUEST_ACTION_REMOVE_CAR = "remove_car";
	public static final String REQUEST_ACTION_LIST_CARS = "list_cars_on_parking";
	public static final String REQUEST_ACTION_GET_RATE = "get_daily_rate";
	public static final String REQUEST_ACTION_CHECK_CAR = "check_car";
	
	public static final String ERROR_MESSAGE_CAR_ALREADY_PARKED = "Car already parked";
	public static final String ERROR_MESSAGE_NO_CAR_AT_PARKING = "No car at parking";
	public static final String ERROR_MESSAGE_CAR_DATA_ERROR_OR_EMPTY = "Car data error or no car data provided";
	public static final String ERROR_MESSAGE_GENERAL_ERROR = "general error";
	
}
