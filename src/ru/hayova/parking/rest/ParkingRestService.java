package ru.hayova.parking.rest;

//**********************************
//* (c) Oleg 'hayova' Nadein, 2016 *
//**********************************

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ru.hayova.parking.model.ParkingRequest;

@Stateless
@Path("/parking")
public class ParkingRestService {

	@EJB
    private ParkingService parkingService;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response processRequest(ParkingRequest request) {
		return ResponseBuilder.success(parkingService.process(request));
	}
	
}
