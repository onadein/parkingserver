package ru.hayova.parking.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import ru.hayova.parking.model.Journal;
import ru.hayova.parking.model.ParkingRequest;
import ru.hayova.parking.model.ParkingResponse;
import ru.hayova.parking.model.ParkingResponse.RatesData;
import ru.hayova.parking.model.ParkingResponse.ResultData;
import ru.hayova.parking.model.ParkingResponse.RatesData.PerDayRatesData;

@Stateless
public class ParkingService {
	
	@EJB
    private DbManager dbManager;
	
	Map<Integer, Boolean> places = new HashMap<Integer, Boolean>();
	
	public ParkingResponse process(ParkingRequest request) {
		ParkingResponse parkingResponse = new ParkingResponse();
		
		if(request != null) {
			if(request.getAction().equalsIgnoreCase(ParkingDefines.REQUEST_ACTION_GET_FREE_SPACE)) {
				parkingResponse = freeSpaceOnParking();
			}
			if(request.getAction().equalsIgnoreCase(ParkingDefines.REQUEST_ACTION_ADD_CAR)) {
				parkingResponse = addCar(request.getCar());
			}
			if(request.getAction().equalsIgnoreCase(ParkingDefines.REQUEST_ACTION_REMOVE_CAR)) {
				parkingResponse = removeCar(request.getCar());
			}
			if(request.getAction().equalsIgnoreCase(ParkingDefines.REQUEST_ACTION_LIST_CARS)) {
				parkingResponse = listCarsOnParking();
			}
			if(request.getAction().equalsIgnoreCase(ParkingDefines.REQUEST_ACTION_GET_RATE)) {
				parkingResponse = parkingDailyRate(request.getRateBeginDate(), request.getRateEndDate());
			}
			if(request.getAction().equalsIgnoreCase(ParkingDefines.REQUEST_ACTION_CHECK_CAR)) {
				parkingResponse = isCarOnParking(request.getCar());
			}
		}

		return parkingResponse;
	}

	private ParkingResponse addCar(Journal car) {
		if(!PSUtils.testCar(car))
			return prepareCarErrorResponce(ParkingDefines.ERROR_MESSAGE_CAR_DATA_ERROR_OR_EMPTY);
		
		ParkingResponse response = isCarOnParking(car);
		
		if(response.getResult().equalsIgnoreCase(ParkingDefines.RESPONSE_RESULT_SUCCESS)) {
			if(response.getResultData() != null && response.getResultData().getInfo().equalsIgnoreCase(Boolean.FALSE.toString())) {
				Journal journal = new Journal();
				journal.setCarGovnum(car.getCarGovnum().trim());
				journal.setCarModel(car.getCarModel().trim());
				journal.setMoveInDate(new Date());
				journal.setPlaceNumber(getFirstFreePlaceNumber());
				dbManager.addJournal(journal);
				response.setResultData(null);
				response.setResult(ParkingDefines.RESPONSE_RESULT_SUCCESS);
			} else {
				response.setResult(ParkingDefines.RESPONSE_RESULT_ERROR);
				ResultData resultData = response.getResultData();
				resultData.setDescription(ParkingDefines.RESPONSE_RESULT_ERROR);
				resultData.setInfo(ParkingDefines.ERROR_MESSAGE_CAR_ALREADY_PARKED);
				response.setResultData(resultData);
			}
		}
		
		return response;
	}
		
	private ParkingResponse removeCar(Journal car) {
		if(!PSUtils.testCar(car))
			return prepareCarErrorResponce(ParkingDefines.ERROR_MESSAGE_CAR_DATA_ERROR_OR_EMPTY);
		
		ParkingResponse response = isCarOnParking(car);
		
		if(response.getResult().equalsIgnoreCase(ParkingDefines.RESPONSE_RESULT_SUCCESS)) {
			if(response.getResultData() != null && response.getResultData().getInfo().equalsIgnoreCase(Boolean.TRUE.toString())) {
				dbManager.updateJournalByCarOut(car, new Date());
				response.setResultData(null);
				response.setResult(ParkingDefines.RESPONSE_RESULT_SUCCESS);
			} else {
				response.setResult(ParkingDefines.RESPONSE_RESULT_ERROR);
				ResultData resultData = response.getResultData();
				resultData.setDescription(ParkingDefines.RESPONSE_RESULT_ERROR);
				resultData.setInfo(ParkingDefines.ERROR_MESSAGE_NO_CAR_AT_PARKING);
				response.setResultData(resultData);
			}
		}
		
		return response;
	}
	
	private ParkingResponse listCarsOnParking() {
		List<Journal> carList = dbManager.getJournalsWithEmptyOut();
		
		ParkingResponse response = new ParkingResponse();
		ResultData resultData = response.new ResultData();
		
		response.setResult(ParkingDefines.RESPONSE_RESULT_SUCCESS);
		resultData.setDescription(ParkingDefines.REQUEST_ACTION_LIST_CARS);
		resultData.setInfo(String.valueOf(carList.size()));
		
		List<Journal> cars = new ArrayList<Journal>();
		for(Journal car : carList)
			cars.add(new Journal(car.getCarGovnum(), car.getCarModel(), car.getMoveInDate()));
		
		resultData.setCars(cars);
		
		response.setResultData(resultData);
		
		return response;
	}
	
	private ParkingResponse freeSpaceOnParking() {
		List<Journal> carList = dbManager.getJournalsWithEmptyOut();
		
		ParkingResponse response = new ParkingResponse();
		ResultData resultData = response.new ResultData();
		
		response.setResult(ParkingDefines.RESPONSE_RESULT_SUCCESS);
		resultData.setDescription(ParkingDefines.REQUEST_ACTION_GET_FREE_SPACE);
		resultData.setInfo(String.valueOf(ParkingDefines.MAX_PARKING_PLACES - carList.size()));
		response.setResultData(resultData);
		
		return response;
	}
	
	private ParkingResponse parkingDailyRate(Date dateBegin, Date dateEnd) {
		ParkingResponse response = new ParkingResponse();
		
		response.setResult(ParkingDefines.RESPONSE_RESULT_SUCCESS);
		
		ResultData resultData = response.new ResultData();
		resultData.setDescription(ParkingDefines.REQUEST_ACTION_GET_RATE);
		resultData.setInfo("");
		
		RatesData ratesData = response.new RatesData();

		if (dateBegin != null && dateEnd != null && dateBegin.compareTo(dateEnd) > 0) {
			dateBegin = null;
			dateEnd = null;
		}
		
		if(dateEnd != null && dateEnd.compareTo(new Date()) > 0)
			dateEnd = null;
		
		if(dateBegin == null)
			dateBegin = new Date();

		if(dateEnd == null)
			dateEnd = new Date();

		long diff = dateBegin.getTime() - dateEnd.getTime();
		long daysDiff = Math.abs(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));

		Date currentStartDate = PSUtils.setDateTimeToBegin(dateBegin);
		Date currentEndDate = PSUtils.setDateTimeToEnd(currentStartDate);
		
		long totalRate = 0;
		long currentDayRate = 0;
		long currentCarHours = 0;
		
		List<PerDayRatesData> ratesPerDataList = new ArrayList<PerDayRatesData>();
		
		for(int i = 0; i<daysDiff+1; i++) {
			currentDayRate = 0;

			List<Journal> rateDate = dbManager.getJournalByDatePeriod(currentStartDate, currentEndDate);
			for(Journal journal : rateDate) {

				currentCarHours = 0;
				if(journal.getMoveInDate() == null && journal.getMoveOutDate() != null) {
					prepareCarErrorResponce(ParkingDefines.ERROR_MESSAGE_GENERAL_ERROR); 
				}
				if(journal.getMoveInDate() != null && journal.getMoveOutDate() == null) {                 
					Date todayWithZeroTime = PSUtils.setDateTimeToBegin(new Date());
					Date moveInDateWithZeroTime = PSUtils.setDateTimeToBegin(journal.getMoveInDate());
					Date currentStartDateWithZeroTime = PSUtils.setDateTimeToBegin(currentStartDate);
					
					if (todayWithZeroTime.compareTo(moveInDateWithZeroTime) == 0)
						diff = (new Date()).getTime() - journal.getMoveInDate().getTime();
					else {
						if (todayWithZeroTime.compareTo(currentStartDateWithZeroTime) == 0)
							diff = (new Date()).getTime() - currentStartDate.getTime();
						else
							diff = currentEndDate.getTime() - journal.getMoveInDate().getTime();
					}
				}
				
				if(journal.getMoveInDate() != null && journal.getMoveOutDate() != null) {
					diff = journal.getMoveOutDate().getTime() - journal.getMoveInDate().getTime();
				}
				
				currentCarHours = (long) Math.ceil(TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS) * 1.0 / 60);
				currentDayRate+=currentCarHours*ParkingDefines.PER_HOUR_RATE_TAX;
			}
			
			if(currentDayRate > 0) {
				PerDayRatesData rerDayRatesData = ratesData.new PerDayRatesData();
				rerDayRatesData.setDate(currentStartDate);
				rerDayRatesData.setRate(currentDayRate);
				
				ratesPerDataList.add(rerDayRatesData);			
				
				totalRate += currentDayRate;
			}
			
			currentStartDate = PSUtils.setDateTimeToBegin(PSUtils.addOneDayToDate(currentStartDate));
			currentEndDate = PSUtils.setDateTimeToEnd(currentStartDate);
		}
		
		ratesData.setTotalRate(totalRate);
		ratesData.setPerDayRates(ratesPerDataList);
		
		response.setResultData(resultData);
		response.setRatesData(ratesData);
		
		return response;
	}
	
	private ParkingResponse isCarOnParking(Journal car) {
		if(!PSUtils.testCar(car))
			return prepareCarErrorResponce(ParkingDefines.ERROR_MESSAGE_CAR_DATA_ERROR_OR_EMPTY);
		
		List<Journal> carList = dbManager.getJournalByCarWithEmptyOut(car);
		
		ParkingResponse response = new ParkingResponse();
		ResultData resultData = response.new ResultData();
		
		if(carList.size() == 1) {
			response.setResult(ParkingDefines.RESPONSE_RESULT_SUCCESS);
			resultData.setDescription(ParkingDefines.REQUEST_ACTION_CHECK_CAR);
			resultData.setInfo(Boolean.TRUE.toString());
			response.setResultData(resultData);
		} else if(carList.size() == 0) {
			response.setResult(ParkingDefines.RESPONSE_RESULT_SUCCESS);
			resultData.setDescription(ParkingDefines.REQUEST_ACTION_CHECK_CAR);
			resultData.setInfo(Boolean.FALSE.toString());
			response.setResultData(resultData);
		} else {
			response.setResult(ParkingDefines.RESPONSE_RESULT_ERROR);
			resultData.setDescription(ParkingDefines.RESPONSE_RESULT_ERROR);
			resultData.setInfo(ParkingDefines.ERROR_MESSAGE_GENERAL_ERROR);
			response.setResultData(resultData);
		}
		
		return response;
	}
	
	private Integer getFirstFreePlaceNumber() {
		refreshPlaces();
		
		for (Map.Entry<Integer, Boolean> entry : places.entrySet()) {
		    if(entry.getValue() == false)
		    	return entry.getKey();
		}
		
		return -1;
	} 
	
	private void refreshPlaces() {
		List<Journal> carsOnParking = dbManager.getJournalsWithEmptyOut();
		
		for(int i=1; i<ParkingDefines.MAX_PARKING_PLACES + 1; i++) {
			places.put(i, false);
		}
		
		for(Journal journal : carsOnParking) {
			places.put(journal.getPlaceNumber(), true);
		}
	}
	
	private ParkingResponse prepareCarErrorResponce(String errorInfo) {
		ParkingResponse response = new ParkingResponse();
		ResultData resultData = response.new ResultData();
		
		response.setResult(ParkingDefines.RESPONSE_RESULT_ERROR);
		resultData.setDescription(ParkingDefines.RESPONSE_RESULT_ERROR);
		resultData.setInfo(errorInfo);
		response.setResultData(resultData);
		
		return response;
	}

}
