package ru.hayova.parking.rest;

//**********************************
//* (c) Oleg 'hayova' Nadein, 2016 *
//**********************************

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class ParkingRestApp extends Application {

}
